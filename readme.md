# Laravel model revision package

## Install
```bash
composer require pilyavskiy/laravel-model-revision
```

## Usage
To start using revisions for a model, you need to run a command, where `Example` is the `base` model.

```bash
php artisan make:model-revision Example
```

After executing the command, a `revision` model `ExampleRevision` and a migration for the revision will be created.

### ! Important
The `revision` model needs:
1. the `$fillable` property needs to be filled in 
2. the base model fields need to be added to the migration.

### ! Important
The `base` model must:
1. Extends `Pilyavskiy\ModelRevision\Models\ModelRevision` or use `Pilyavskiy\ModelRevision\Trait\Revision` 
2. The `$revision` property is filled with the path to the revision model.

```php
public string $revision = \App\Models\Revisions\ExampleRevision::class;
```
