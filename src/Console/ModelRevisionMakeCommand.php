<?php

namespace Pilyavskiy\ModelRevision\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ModelRevisionMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:model-revision  {name : Base Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Revision eloquent model class';

    public function handle(): void
    {
        if (empty($this->argument('name'))) {
            $this->error('Name is required param');
        }
        $modelName = ucfirst($this->argument('name'));
        $this->createModel($modelName);
        $this->createMigration($modelName);
    }

    protected function createModel(string $modelName): void
    {
        if (! file_exists(app_path('Models/Revisions'))) {
            @mkdir(app_path('Models/Revisions'), 0777, true);
        }

        $modelContent = file_get_contents(__DIR__.'/stubs/model.stub');
        $modelContent = str_replace(
            [
                'DummyNamespace',
                'DummyClass',
                'dummyTableName',
            ],
            [
                'App\\Models\\Revisions',
                $this->getRevisionModelName($modelName),
                $this->getTableName($modelName),
            ],
            $modelContent
        );

        $file = fopen(app_path(sprintf('Models/Revisions/%s.php', $this->getRevisionModelName($modelName))), 'w');
        fwrite($file, $modelContent);
        fclose($file);
    }

    protected function createMigration(string $modelName): void
    {
        $modelContent = file_get_contents(__DIR__.'/stubs/migration.stub');
        $modelContent = str_replace(
            'dummyTableName',
            $this->getTableName($modelName),
            $modelContent
        );

        $file = fopen(
            base_path(sprintf('database/migrations/%s_%s_table.php', $this->getDatePrefix(), $this->getTableName($modelName))),
            'w'
        );
        fwrite($file, $modelContent);
        fclose($file);
    }

    protected function getDatePrefix(): string
    {
        return date('Y_m_d_His');
    }

    protected function getTableName(string $modelName): string
    {
        return sprintf('%s_revisions', Str::snake($modelName));
    }

    protected function getRevisionModelName(string $modelName): string
    {
        return sprintf('%sRevision', $modelName);
    }
}
