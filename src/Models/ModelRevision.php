<?php

namespace Pilyavskiy\ModelRevision\Models;

use Illuminate\Database\Eloquent\Model;
use Pilyavskiy\ModelRevision\Trait\Revision;

abstract class ModelRevision extends Model
{
    use Revision;

    const CREATED_AT = 'revision_created_at';

    const UPDATED_AT = null;

    /**
     * @var string Model that save revision with state
     *             Should contain all fields from base model, revision and model_id
     *
     * @example \App\Models\Revisions\ConfigRevision::class
     */
    public string $revision;
}
