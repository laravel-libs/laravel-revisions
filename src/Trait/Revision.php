<?php

namespace Pilyavskiy\ModelRevision\Trait;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Pilyavskiy\ModelRevision\Events\ModelUpdated;

trait Revision
{
    protected static function bootRevision(): void
    {
        static::updated(static function (Model $model) {
            new ModelUpdated($model);
        });
    }

    public function getRevisions(): Collection
    {
        if (empty($this->id)) {
            return new Collection();
        }

        return $this->revision::where('model_id', $this->id)
            ->orderBy('revision', 'DESC')
            ->get();
    }

    public function getLatestRevision(): ?Model
    {
        if (empty($this->id)) {
            return null;
        }

        return $this->revision::where('model_id', $this->id)
            ->orderBy('revision', 'DESC')
            ->first();
    }

    public function findRevision(int $revision): Model
    {
        if (! $this->isRevisionExists($revision)) {
            throw new \RuntimeException(
                sprintf('Revision number %s does not exist in the model %s', $revision, $this->revision)
            );
        }

        return $this->revision::where('model_id', $this->id)
            ->where('revision', $revision)
            ->first();
    }

    public function restoreRevision(int $revision): void
    {
        $existingRevision = $this->findRevision($revision);

        $this->update(
            $existingRevision->makeHidden(['id', 'revision', 'model_id'])->toArray()
        );
    }

    public function isRevisionExists(int $revision): bool
    {
        if (empty($this->id)) {
            return false;
        }

        return $this->revision::where('model_id', $this->id)
            ->where('revision', $revision)
            ->exists();
    }
}
