<?php

namespace Pilyavskiy\ModelRevision\Providers;

use Illuminate\Support\ServiceProvider;
use Pilyavskiy\ModelRevision\Console\ModelRevisionMakeCommand;

class ModelRevisionServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ModelRevisionMakeCommand::class,
            ]);
        }
    }
}
