<?php

namespace Pilyavskiy\ModelRevision\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ModelUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(Model $model)
    {
        if (! empty($model->revision) && class_exists($model->revision) && count($model->getChanges()) && $this->hasNotIgnoredChanges($model) && auth()->check()) {
            $model->revision::create(
                array_merge(
                    [
                        'id' => null,
                        'model_id' => $model->id,
                        'revision' => ($model->getLatestRevision()?->revision ?? 0) + 1,
                        'revision_changes' => $model->getChanges(),
                        'revision_note' => null,
                        'revision_created_by' => auth()?->user()?->id ?? null,
                    ],
                    $model->getOriginal()
                )
            );
        }
    }

    private function hasNotIgnoredChanges(Model $model): bool
    {
        if (empty($model->ignoreRevisionFields) || ! is_array($model->ignoreRevisionFields)) {
            return true;
        }

        foreach ($model->getChanges() as $field => $value) {
            if (! in_array($field, $model->ignoreRevisionFields, true)) {
                return true;
            }
        }

        return false;
    }
}
